import sys
from optparse import OptionParser
src_path = "/Users/jordi/software/FORESEE/"
sys.path.append(src_path)

import numpy as np
from src.foresee import Foresee, Utility, Model

from matplotlib import pyplot as plt


parser = OptionParser()
parser.add_option('-m','--mass', help='ALP mass', type=float, default=0.35)
(options, args) = parser.parse_args()

ALPmass = options.mass

foresee = Foresee()

energy = "13.6"
modelname = "UpPhilic"
model = Model(modelname)

model.add_production_2bodydecay(
    pid0 = "221",
    pid1 = "111",
    br = "1.26e5 * coupling**2 * np.sqrt((1-(mass+0.135)**2/0.547**2)*(1-(mass-0.135)**2/0.547**2))",
    generator = "EPOSLHC",
    energy = energy,
    nsample = 10,
)

model.add_production_2bodydecay(
    pid0 = "331",
    pid1 = "111",
    br = "273. * coupling**2 * np.sqrt((1-(mass+0.135)**2/0.957**2)*(1-(mass-0.135)**2/0.957**2))",
    generator = "EPOSLHC",
    energy = energy,
    nsample = 10,
)

model.add_production_2bodydecay(
    pid0 = "321",
    pid1 = "211",
    br = "7.42 * coupling**2 * np.sqrt((1-(mass+0.135)**2/0.49368**2)*(1-(mass-0.135)**2/0.49368**2))",
    generator = "EPOSLHC",
    energy = energy,
    nsample = 10,
)

model.add_production_2bodydecay(
    pid0 = "-321",
    pid1 = "-211",
    br = "7.42 * coupling**2 * np.sqrt((1-(mass+0.135)**2/0.49368**2)*(1-(mass-0.135)**2/0.49368**2))",
    generator = "EPOSLHC",
    energy = energy,
    nsample = 10,
)

model.set_ctau_1d(
    filename="model/ctau.txt", 
    coupling_ref=1
)

branchings = [
    ["gamma"     , "black"        , "solid" , r"$\gamma\gamma$"         , 0.110, 0.50],
    ["pi0_pi0"   , "red"          , "solid" , r"$\pi^0\pi^0$"          , 0.175, 0.03],
    ["pi+_pi-"   , "blue"         , "solid" , r"$\pi^+\pi^-$"           , 0.300, 0.03],
]

model.set_br_1d(
    modes=[channel for channel,_,_,_,_,_ in branchings],
    finalstates=[[22,22],[111,111],],
    filenames=["model/br/"+channel+".txt" for channel,_,_,_,_,_ in branchings],
)

foresee.set_model(model=model)


#masses = np.logspace(-2,-0.3,18)
masses = np.logspace(-1,-0.3,23)
for mass in masses:
    foresee.get_llp_spectrum(mass=mass,coupling=1)


distance = 480
length = 1.5 + 2 + 0.5
channels = ["gamma","pi0_pi0"] # None for all the channels
#channels = ["gamma"] # None for all the channels
#channels = None #for all the channels
luminosity = 3000
selection = "np.sqrt(x.x**2 + (x.y - 0.065)**2)< 0.11"
foresee.set_detector(distance=distance, selection=selection, length=length, luminosity=luminosity, channels=channels)



couplings = np.logspace(-10,-2,17)
print(couplings)

seed = 1234
nbinsample = 100
n_events = 1000




# set z_front to -length so the particles are shot in the right z position in allpix
z_front = -length # -1.5 in Calpyso

for mass in masses:
    print('\nDoing mass',mass)
    output = foresee.get_events(mass=mass, energy=energy, couplings = couplings)
    coups, ctaus, nevents, momenta, weights = output
    for coup,ctau,nsig in zip(coups, ctaus, nevents):
        print ("epsilon =", '{:5.3e}'.format(coup), ": nsig =", '{:5.3e}'.format(nsig))
    
    integer_part, decimal_part = str(round(mass,3)).split(".")
    smass = integer_part + "p" + decimal_part
    for icoup,coup in enumerate(couplings):
        # transform 0.001 to 1.0e-3
        scoup_tmp = '{:5.1e}'.format(coup)
        integer, decimal = str(scoup_tmp).split(".")
        scoup = integer + "p" + decimal
        print(' Doing coupling','{:5.1e}'.format(coup))   
        print('nevents = ',nevents[icoup])
        #if (nevents[icoup] < 1e-2): 
        #    continue
        fileName = "model/events/"+modelname+"_m"+smass+"_eps"+scoup+".hepmc"
        #fileName = "model/events/U1B"+"_m"+smass+"_eps"+scoup+".hepmc"
        print(fileName)
        foresee.write_events(mass=mass, coupling=coup, energy=energy, filename=fileName, 
                            numberevent=n_events, zfront=z_front, seed = seed, decaychannels = channels, notime = True, t0 = 0, nsample=nbinsample, return_data = True)
                            #numberevent=n_events, zfront=z_front, seed = seed, decaychannels = channels, notime = False, t0 = 0, nsample=nbinsample, return_data = True, extend_to_low_pt_scales={"5":1, "-5":1})




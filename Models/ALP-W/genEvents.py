import numpy as np
import sys
import os
from optparse import OptionParser
src_path = "/Users/jordi/software/FORESEE/"
sys.path.append(src_path)
from src.foresee import Foresee, Utility, Model


parser = OptionParser()
parser.add_option('-m','--mass', help='ALP mass', type=float, default=0.2)
(options, args) = parser.parse_args()

ALPmass = options.mass


foresee = Foresee()

energy = "13.6"
#energy = "14"
modelname="ALP-W"
model = Model(modelname)




model.add_production_2bodydecay(
    pid0 = "5",
    pid1 = "321",
    br = "2.2e4 * coupling**2 * np.sqrt((1-(mass+0.495)**2/5.279**2)*(1-(mass-0.495)**2/5.279**2))",
    generator = "Pythia8",
    energy = energy,
    nsample = 100,
    preselectioncut = "p>10",
)

model.add_production_2bodydecay(
    pid0 = "-5",
    pid1 = "321",
    br = "2.2e4 * coupling**2 * np.sqrt((1-(mass+0.495)**2/5.279**2)*(1-(mass-0.495)**2/5.279**2))",
    generator = "Pythia8",
    energy = energy,
    nsample = 100,
    preselectioncut = "p>10",
)

model.add_production_2bodydecay(
    pid0 = "130",
    pid1 = "111",
    br = "4.5 * coupling**2 * np.sqrt((1-(mass+0.135)**2/0.495**2)*(1-(mass-0.135)**2/0.495**2))",
    generator = "EPOSLHC",
    energy = energy,
    nsample = 20,
)

model.add_production_2bodydecay(
    pid0 = "321",
    pid1 = "211",
    br = "10.5 * coupling**2 * np.sqrt((1-(mass+0.135)**2/0.495**2)*(1-(mass-0.135)**2/0.495**2))",
    generator = "EPOSLHC",
    energy = energy,
    nsample = 20,
)

model.add_production_2bodydecay(
    pid0 = "-321",
    pid1 = "211",
    br = "10.5 * coupling**2 * np.sqrt((1-(mass+0.135)**2/0.495**2)*(1-(mass-0.135)**2/0.495**2))",
    generator = "EPOSLHC",
    energy = energy,
    nsample = 20,
)


model.set_ctau_1d(
    filename="model/ctau.txt", 
    coupling_ref=1
)

model.set_br_1d(
    modes=["gamma_gamma"],
    finalstates=[[22,22]],
    filenames=["model/br/gamma_gamma.txt"]
)


foresee.set_model(model=model)


masses = np.logspace(-1.5,0.3,15)
print('masses',masses)
'''
masses = [ 
    0.01  ,  0.0126,  0.0158,  0.02  ,  0.0251,  0.0316,  0.0398,
    0.0501,  0.0631,  0.0794,  0.1   ,  0.1122,  0.1259,  0.1413,
    0.1585,  0.1778,  0.1995,  0.2239,  0.2512,  0.2818,  0.3162,
    0.3548,  0.3981,  0.4467,  0.5012,  0.5623,  0.6026,  0.631 ,
    0.6457,  0.6607,  0.6761,  0.6918,  0.7079,  0.7244,  0.7413,
    0.7586,  0.7762,  0.7943,  0.8128,  0.8318,  0.8511,  0.871 ,
    0.8913,  0.912 ,  0.9333,  0.955 ,  0.9772,  1.    ,  1.122 ,
    1.2589,  1.4125,  1.5849,  1.7783,  1.9953,  2.2387,  2.5119,
    2.8184,  3.1623,  3.9811,  5.0119,  6.3096,  7.9433, 10.    
]
'''

#masses = [2.9,3,3.1,3.2,3.3,3.5,3.7,3.9,4,4.2,4.35,4.5]
#masses = [ALPmass]
for mass in masses:
    foresee.get_llp_spectrum(mass=mass,coupling=1)





distance = 480
# Since not relying on tracks then extend length to include spectrometer (magents + tracking stations)
length = 1.5 + 2 + 0.5
channels = ["gamma_gamma"] # None for all the channels
luminosity = 3000
# Generate 6.5 cm high to account for translation from ATLAS to FASER coord. system
#selection = "np.sqrt(x.x**2 + x.y**2)< .11"
selection = "np.sqrt(x.x**2 + (x.y - 0.065)**2)< 0.11"
foresee.set_detector(distance=distance, selection=selection, length=length, luminosity=luminosity, channels=channels)



#couplings = np.logspace(-8,-2,7)
#couplings = np.logspace(-8,-3,6)
#couplings = np.logspace(-8,-2,7)
#couplings = np.logspace(-7,-2,6)
couplings = np.logspace(-7,-2,21)
print('couplings',couplings)




seed = 1234
nbinsample = 100
n_events = 1000



n_files = 10

# set z_front to -length so the particles are shot in the right z position in allpix
z_front = -length # -1.5 in Calpyso

for mass in masses:
    print('\nDoing mass',mass)
    output = foresee.get_events(mass=mass, energy=energy, couplings = couplings, )
    coups, ctaus, nevents, momenta, weights = output
    for coup,ctau,nsig in zip(coups, ctaus, nevents):
        print ("epsilon =", '{:5.3e}'.format(coup), ": nsig =", '{:5.3e}'.format(nsig))
    integer_part, decimal_part = str(round(mass,3)).split(".")
    smass = integer_part + "p" + decimal_part
    for icoup,coup in enumerate(couplings):
        # transform 0.001 to 1.0e-3
        scoup_tmp = '{:5.1e}'.format(coup)
        integer, decimal = str(scoup_tmp).split(".")
        scoup = integer + "p" + decimal
        print(' Doing coupling','{:5.1e}'.format(coup))   
        print('nevents = ',nevents[icoup])
        #for ifile in range(n_files):
            #seed = ifile
            #fileName = "model/events/"+modelname+"_m"+smass+"_eps"+scoup+"_"+str(ifile+1)+".hepmc"
        fileName = "model/events/"+modelname+"_m"+smass+"_eps"+scoup+".hepmc"
        print(fileName)
        foresee.write_events(mass=mass, coupling=coup, energy=energy, filename=fileName, 
                            numberevent=n_events, zfront=z_front, seed = seed, decaychannels = channels, notime = True, t0 = 0, nsample=nbinsample, return_data = True)
                            #numberevent=n_events, zfront=z_front, seed = seed, decaychannels = channels, notime = False, t0 = 0, nsample=nbinsample, return_data = True, extend_to_low_pt_scales={"5":1, "-5":1})



#f = open(fileName, 'r')
#file_contents = f.read()
#print (file_contents)
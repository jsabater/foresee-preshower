import numpy as np
import matplotlib.pyplot as plt

#masses = np.logspace(-1.5, 0.3, 15)
#couplings = np.logspace(-7, -2, 21)

# ALP-photon
#couplings = np.logspace(-6,-2,17)
#masses = np.logspace(-2,-0.3,20+1)

# U(1)B
#couplings = np.logspace(-8,-2,25)
#couplings = np.logspace(-8,-2,19)
#masses = np.logspace(-1,0,21)


# UpPhilic
masses = np.logspace(-1,-0.3,23)
couplings = np.logspace(-10,-2,17)


# Create a meshgrid of mass and coupling values
mass_grid, coupling_grid = np.meshgrid(masses, couplings)



# Flatten the grids to create X and Y arrays for plotting
masses_flattened = mass_grid.flatten()
couplings_flattened = coupling_grid.flatten()

# Plotting
plt.scatter(masses_flattened, couplings_flattened)
plt.xscale('log')
plt.yscale('log')
plt.xlabel('Masses')
plt.ylabel('Couplings')
plt.title('Masses vs. Couplings')
plt.grid(True)
plt.show()
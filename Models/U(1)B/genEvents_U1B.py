import numpy as np
import sys
import os
from optparse import OptionParser
src_path = "/Users/jordi/software/FORESEE/"
sys.path.append(src_path)
from src.foresee import Foresee, Utility, Model

foresee = Foresee()

energy = "13.6"
modelname="U(1)B"
model = Model(modelname, path="./")

model.add_production_2bodydecay(
    pid0 = "111",
    pid1 = "22",
    br = "2*0.99 * (coupling/0.3)**2 * pow(1.-pow(mass/self.masses('111'),2),3)*np.abs((1-mass**2/0.78265**2 - 0.00849/0.78265*1j)**(-1))**2",
    generator = "EPOSLHC",
    energy = energy,
    nsample = 10
)

model.add_production_2bodydecay(
    pid0 = "221",
    pid1 = "22",
    br = "1./16*0.3941 * (coupling/0.3)**2 * pow(1.-pow(mass/self.masses('221'),2),3)*np.abs((1-mass**2/0.78265**2 - 0.00849/0.78265*1j)**(-1)+(1-mass**2/1.019461**2-0.004249/1.019461*1j)**(-1))**2",
    generator = "EPOSLHC",
    energy = energy,
    nsample = 10, 
)

model.add_production_2bodydecay(
    pid0 = "331",
    pid1 = "22",
    br = "1./49.*0.02307 * (coupling/0.3)**2 * pow(1.-pow(mass/self.masses('331'),2),3) * np.abs((1-mass**2/0.78265**2 - 0.00849/0.78265*1j)**(-1) - 2*(1-mass**2/1.019461**2-0.004249/1.019461*1j)**(-1))**2",
    generator = "EPOSLHC",
    energy = energy,
    nsample = 10, 
)

model.add_production_mixing(
    pid = "223",
    mixing = "coupling * 4./17. * 0.78265**2/abs(mass**2-0.78265**2+0.78265*0.00849*1j)",
    generator = "EPOSLHC",
    energy = energy,
)

model.add_production_mixing(
    pid = "333",
    mixing = "coupling /12.88 * 1.019461**2/abs(mass**2-1.019461**2+1.019461*0.004249*1j)",
    generator = "EPOSLHC",
    energy = energy,
)

masses_brem = [ 
   0.01  ,  0.0126,  0.0158,  0.02  ,  0.0251,  0.0316,  0.0398,
   0.0501,  0.0631,  0.0794,  0.1   ,  0.1122,  0.1259,  0.1413,
   0.1585,  0.1778,  0.1995,  0.2239,  0.2512,  0.2818,  0.3162,
   0.3548,  0.3981,  0.4467,  0.5012,  0.5623,  0.6026,  0.631 ,
   0.6457,  0.6607,  0.6761,  0.6918,  0.7079,  0.7244,  0.7413,
   0.7586,  0.7762,  0.7943,  0.8128,  0.8318,  0.8511,  0.871 ,
   0.8913,  0.912 ,  0.9333,  0.955 ,  0.9772,  1.    ,  1.122 ,
   1.2589,  1.4125,  1.5849,  1.7783,  1.9953,  2.2387,  2.5119,
   2.8184,  3.1623,  3.9811,  5.0119,  6.3096,  7.9433, 10.    
]

model.add_production_direct(
    label = "Brem",
    energy = energy,
    condition = "1./0.3**2 * (p.pt<1)",
    coupling_ref=1,
    masses = masses_brem,
)

model.set_ctau_1d(
    filename="model/ctau.txt", 
    coupling_ref=1
)

branchings = [
    ["pigamma", "black"        , "solid" , r"$\pi\gamma$"         , 0.225, 0.50],
    ["3pi"    , "red"          , "solid" , r"$\pi^+\pi^-\pi^0$"   , 0.525, 0.03],
    ["KK"     , "magenta"      , "solid" , r"$KK$"                , 0.905, 0.03],
    ["ee"     , "blue"         , "solid" , r"$ee$"                , 0.110, 0.50],
    ["mumu"   , "dodgerblue"   , "solid" , r"$\mu\mu$"            , 0.205, 0.013],
]

model.set_br_1d(
    modes=np.array(branchings).T[0],
    finalstates=[[111,22], [211,-211,111], [321,-321],[11,-11],[13,-13]],
    filenames=["model/br/br_"+channel+".txt" for channel,_,_,_,_,_ in branchings],
)


foresee.set_model(model=model)


masses = np.logspace(-1,0,21)
#for mass in masses:
#    foresee.get_llp_spectrum(mass=mass,coupling=1)

distance = 480
length = 1.5 + 2 + 0.5
channels = ["pigamma","ee"]
#channels = None
luminosity = 3000
#selection = "np.sqrt(x.x**2 + x.y**2)< 1"
selection = "np.sqrt(x.x**2 + (x.y - 0.065)**2)< 0.11"
#foresee.set_detector(distance=distance, selection=selection, length=length, luminosity=luminosity, channels=channels)

couplings = np.logspace(-8,-2,19)
print(couplings)

seed = 1234
nbinsample = 100
n_events = 1000




# set z_front to -length so the particles are shot in the right z position in allpix
z_front = -length # -1.5 in Calpyso

for mass in masses:
    if mass < 0.3:
        print('SKIP!')
        continue
    print('\nDoing mass',mass)
    output = foresee.get_events(mass=mass, energy=energy, couplings = couplings)
    coups, ctaus, nevents, momenta, weights = output
    for coup,ctau,nsig in zip(coups, ctaus, nevents):
        print ("epsilon =", '{:5.3e}'.format(coup), ": nsig =", '{:5.3e}'.format(nsig))
    
    integer_part, decimal_part = str(round(mass,3)).split(".")
    smass = integer_part + "p" + decimal_part
    for icoup,coup in enumerate(couplings):
        # transform 0.001 to 1.0e-3
        scoup_tmp = '{:5.1e}'.format(coup)
        integer, decimal = str(scoup_tmp).split(".")
        scoup = integer + "p" + decimal
        print(' Doing coupling','{:5.1e}'.format(coup))   
        print('nevents = ',nevents[icoup])
        #if (nevents[icoup] < 1e-2): 
        #    continue
        fileName = "model/events/"+modelname+"_m"+smass+"_eps"+scoup+".hepmc"
        #fileName = "model/events/U1B"+"_m"+smass+"_eps"+scoup+".hepmc"
        print(fileName)
        foresee.write_events(mass=mass, coupling=coup, energy=energy, filename=fileName, 
                            numberevent=n_events, zfront=z_front, seed = seed, decaychannels = channels, notime = True, t0 = 0, nsample=nbinsample, return_data = True)
                            #numberevent=n_events, zfront=z_front, seed = seed, decaychannels = channels, notime = False, t0 = 0, nsample=nbinsample, return_data = True, extend_to_low_pt_scales={"5":1, "-5":1})


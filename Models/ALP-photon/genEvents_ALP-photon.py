import numpy as np
import sys
import os
from optparse import OptionParser
src_path = "/Users/jordi/software/FORESEE/"
sys.path.append(src_path)
from src.foresee import Foresee, Utility, Model


parser = OptionParser()
parser.add_option('-m','--mass', help='ALP mass', type=float, default=0.2)
(options, args) = parser.parse_args()

ALPmass = options.mass


foresee = Foresee()

energy = "13.6"
#energy = "14"
modelname="ALP-photon"
model = Model(modelname)



masses = [float(x) for x in ['{:0.4e}'.format(m) for m in np.logspace(-2,0,20+1)]]
model.add_production_direct(
    label = "Prim",
    energy = energy,
    coupling_ref=1,
    masses=masses,
)

model.set_ctau_1d(
    filename="model/ctau.txt", 
    coupling_ref=1
)


branchings = [
    ["gamma"     , "black"        , "solid" , r"$\gamma\gamma$"         , 0.110, 0.50],
    ["eegamma"   , "red"          , "solid" , r"$ee\gamma$"             , 0.225, 0.03],
]

model.set_br_1d(
    modes=[channel for channel,_,_,_,_,_ in branchings],
    finalstates=[[22,22], [11,-11, 22]],
    filenames=["model/br/"+channel+".txt" for channel,_,_,_,_,_ in branchings],
)


foresee.set_model(model=model)

#masses = np.logspace(-2,0,40+1)
#masses = np.logspace(-2,0,20+1)
masses = np.logspace(-2,-0.3,20+1)
#masses = [ALPmass]

for mass in masses:
    foresee.get_llp_spectrum(mass=mass,coupling=1)



#distance, selection, length, luminosity, channels = 480, "np.sqrt(x.x**2 + x.y**2)< 1", 5, 3000, None
distance = 480-140
length = 1.5 + 2 + 0.5
channels = ["gamma"] # None for all the channels
luminosity = 3000
#selection = "np.sqrt(x.x**2 + (x.y - 0.065)**2)< 0.11"
selection = "np.sqrt(x.x**2 + (x.y - 0.065)**2)< 0.11*340./480."
foresee.set_detector(distance=distance, selection=selection, length=length, luminosity=luminosity, channels=channels)

#couplings = np.logspace(-8,-3,6)
#couplings = np.logspace(-8,-2,7)
couplings = np.logspace(-6,-2,17)


print(couplings)





seed = 1234
nbinsample = 100
#n_events = 10000
n_events = 1000



n_files = 10

# set z_front to -length so the particles are shot in the right z position in allpix
z_front = -length # -1.5 in Calpyso

for mass in masses:
    print('\nDoing mass',mass)
    output = foresee.get_events(mass=mass, energy=energy, couplings = couplings)
    coups, ctaus, nevents, momenta, weights = output
    for coup,ctau,nsig in zip(coups, ctaus, nevents):
        print ("epsilon =", '{:5.3e}'.format(coup), ": nsig =", '{:5.3e}'.format(nsig))
    
    integer_part, decimal_part = str(round(mass,3)).split(".")
    smass = integer_part + "p" + decimal_part
    for icoup,coup in enumerate(couplings):
        # transform 0.001 to 1.0e-3
        scoup_tmp = '{:5.1e}'.format(coup)
        integer, decimal = str(scoup_tmp).split(".")
        scoup = integer + "p" + decimal
        print(' Doing coupling','{:5.1e}'.format(coup))   
        print('nevents = ',nevents[icoup])
        #if (nevents[icoup] < 1e-12): 
        #    continue
        #for ifile in range(n_files):
            #seed = ifile
            #fileName = "model/events/"+modelname+"_m"+smass+"_eps"+scoup+"_"+str(ifile+1)+".hepmc"
        fileName = "model/events/"+modelname+"_m"+smass+"_eps"+scoup+".hepmc"
        print(fileName)
        foresee.write_events(mass=mass, coupling=coup, energy=energy, filename=fileName, 
                            numberevent=n_events, zfront=z_front, seed = seed, decaychannels = channels, notime = True, t0 = 0, nsample=nbinsample, return_data = True)
                            #numberevent=n_events, zfront=z_front, seed = seed, decaychannels = channels, notime = False, t0 = 0, nsample=nbinsample, return_data = True, extend_to_low_pt_scales={"5":1, "-5":1})



#f = open(fileName, 'r')
#file_contents = f.read()
#print (file_contents)